
Установка
-------------------
Предполагается что git и composer установлены глобально

В каталоге DOCUMENT_ROOT выполнить
````
git clone https://JackDavyd@bitbucket.org/private_command/apples.git .
composer install
php init
````
Подключить пустую базу данных в файле /common/config/main-local.php

В каталоге DOCUMENT_ROOT выполнить
````
php yii migrate
````
Должно заработать

Если руками, то в архиве apples.zip собранное приложение, распаковать в корень.
В каталоге /data бекап базы данных

для входа

пароль admin

логин gfhjkm

Можно посмотреть в натуре

http://my-test245.000webhostapp.com/admin

Доступы те же

Периоды между на дереве - упало - сгнило 5 минут. 5 часов для теста много, не ждать же 5 часов чтобы проверить. 
Переключить на 5 часов, только переменную поменять.

Вся логика в модели /backend/models/Apples