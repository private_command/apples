<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use backend\models\Apples;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apples-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Сгенерировать', ['generate'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'color',
            //'size',
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'size',
                'refreshGrid' => true,
                'editableOptions' => [
                    'header' => 'Целостность',
                    'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                    'formOptions' => ['action' => ['sizeupdate']], // point to the new action
                ],
                'value' => function ($model) {
                    return number_format($model->size, 2);
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'state',
                'refreshGrid' => true,
                'editableOptions' => [
                    'header' => 'Статус',
                    'formOptions' => ['action' => ['statusupdate']], // point to the new action
                    'format' => \kartik\editable\Editable::FORMAT_BUTTON,
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data'=>Apples::getStatusList(),
                ],
                'value' => function ($model) {
                        return Apples::STATUSES[$model->state];
                },
                'filter' => Apples::STATUSES,
            ],
            [
                'attribute' => 'start_date',
                'format' => ['date', 'php:d-m-Y H:i:s']
            ],
            [
                'attribute' => 'end_date',
                'format' => ['date', 'php:d-m-Y H:i:s']
            ],

            [
                'attribute' => 'id',
                'width' => '5%',
            ]
        ],
    ]); ?>

    <?php //Pjax::end(); ?>

</div>
