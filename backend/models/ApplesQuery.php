<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Apples]].
 *
 * @see Apples
 */
class ApplesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[state]]=1');
    }

    /**
     * {@inheritdoc}
     * @return Apples[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Apples|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Заполнение таблицы данными
     * @return bool
     * @throws \yii\db\Exception
     */
    public function generate()
    {
        $tmp = $this->getTableNameAndAlias();
        $tablename = reset($tmp);
        $count = rand(5, 50);
        $rows = [];
        $fields = ['color', 'size', 'state', 'start_date', 'end_date'];
        $colors = ['green', 'red', 'yellow', 'orange'];
        for ($i = 1; $i <= $count; $i++) {
            $rows[] = [$colors[array_rand($colors)], 1.0, 1, time() + rand(1, 50) * 100, null];
        }
        $transaction = $this->createCommand()->db->beginTransaction();
        try {
            $this->createCommand()->truncateTable($tablename)->execute();
            $this->createCommand()->batchInsert($tablename, $fields, $rows)->execute();
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }
}
