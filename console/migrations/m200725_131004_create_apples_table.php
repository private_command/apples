<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the creation of table `{{%apples}}`.
 */
class m200725_131004_create_apples_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apples}}', [
            'id' => $this->primaryKey(),
            'color' =>$this->char(10)->notNull(),
            'size' => $this->decimal(3,2)->notNull(),
            'state'=> $this->tinyInteger(1)->notNull()->defaultValue(1),
            'start_date' => $this->integer(11)->notNull(),
            'end_date' => $this->integer(11),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apples}}');
    }
}
