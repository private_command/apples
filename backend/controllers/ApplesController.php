<?php

namespace backend\controllers;

use backend\models\ApplesQuery;
use Yii;
use backend\models\Apples;
use backend\models\ApplesSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\grid\EditableColumnAction;
use yii\helpers\ArrayHelper;

/**
 * ApplesController implements the CRUD actions for Apples model.
 */
class ApplesController extends Controller
{
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'statusupdate' => [
                'class' => EditableColumnAction::class,
                'modelClass' => Apples::class,
                'outputValue' => function ($model, $attribute, $key, $index) {
                    return Apples::STATUSES[$model->$attribute];
                },

            ],
            'sizeupdate' => [
                'class' => EditableColumnAction::class,
                'modelClass' => Apples::class,
                'outputValue' => function ($model, $attribute, $key, $index) {
                    return round($model->$attribute,2);
                },

            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'sizeupdate', 'statusupdate', 'generate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'sizeupdate' => ['POST'],
                    'statusupdate' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Apples models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * генератор тестовых данных
     * @return \yii\web\Response
     */
    public function actionGenerate()
    {
        $model = new ApplesQuery(Apples::class);
        if ($model->generate()) {
            Yii::$app->session->setFlash('success', 'Тестовые данные созданы');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка транзакции');
        }
        return $this->redirect(['index']);
    }
}
